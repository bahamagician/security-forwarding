﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Security Forwarding Service Ltd. - Bahamas Custom Brokerage - Freight Forwarding - Packing</title>
<META NAME="DESCRIPTION" CONTENT="Security Forwarding Service is a Bahamian company that offers customs brokerage, freight forwarding, packing, crating and moving services into, out of and throughout the Bahamas.">
<META NAME="KEYWORDS" CONTENT="Security Forwarding Service,Bahamas customs brokerage,Bahamas freight forwarding,bahamas packing,Bahamas crating, Bahamas moving services">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
td img {display: block;}body {
	background-color: #EFEFEF;
	margin-top: 0px;
}
</style>
<!--Fireworks CS3 Dreamweaver CS3 target.  Created Wed Jul 16 14:18:07 GMT-0400 (Eastern Daylight Time) 2008-->
<link href="stylesheets/styleSheet1.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {color: #2A3A91}
.style2 {color: #990000}
-->
</style>
<script type="text/javascript">
<!--
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
//-->
</script>
</head>
<body>
<table width="0" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- fwtable fwsrc="forFireworks.png" fwpage="Page 1" fwbase="index.gif" fwstyle="Dreamweaver" fwdocid = "15021351" fwnested="1" -->
  <tr>
   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="747">
	  <tr>
	   <td><img name="index_r1_c2" src="images/index_r1_c2.gif" width="747" height="8" border="0" id="index_r1_c2" alt="bahamas freight forwarding and packing graphic" /></td>
	  </tr>
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="747">
		  <tr>
		   <td><img name="index_r2_c2" src="images/index_r2_c2.gif" width="407" height="104" border="0" id="index_r2_c2" alt="Bahamas Customs Brokerage Logo" /></td>
		   <td><img name="index_r2_c6" src="images/index_r2_c6.gif" width="116" height="104" border="0" id="index_r2_c6" alt="bahamas freight forwarding and packing graphic" /></td>
		   <td><img name="index_r2_c7" src="images/index_r2_c7.gif" width="224" height="104" border="0" id="index_r2_c7" alt="Bahamas Freight Forwarding Number" /></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="747">
		  <tr>
		   <td><img name="index_r3_c2" src="images/index_r3_c2.jpg" width="407" height="154" border="0" id="index_r3_c2" alt="Bahamas customs brokerage motto" /></td>
		   <td><img name="index_r3_c6" src="images/index_r3_c6.jpg" width="116" height="154" border="0" id="index_r3_c6" alt="freight forwarding globe" /></td>
		   <td><img name="index_r3_c7" src="images/index_r3_c7.jpg" width="224" height="154" border="0" id="index_r3_c7" alt="packing globe" /></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="747">
		  <tr>
		   <td><img name="index_r4_c2" src="images/index_r4_c2.jpg" width="291" height="21" border="0" id="index_r4_c2" alt="bahamas freight forwarding and packing graphic" /></td>
		   <td width="116" background="images/index_r4_c5.gif"><div align="center"><a href="index.html">About Us </a></div></td>
		   <td width="116" background="images/index_r4_c6.gif"><div align="center"><a href="services.html">Our Services </a></div></td>
		   <td width="116" background="images/index_r4_c7.gif"><div align="center"><a href="contact.php">Contact Us </a></div></td>
		   <td><img name="index_r4_c8" src="images/index_r4_c8.jpg" width="108" height="21" border="0" id="index_r4_c8" alt="bahamas freight forwarding and packing graphic" /></td>
		  </tr>
		</table></td>
	  </tr>
	  <tr>
	   <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="747">
		  <tr>
		   <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
             <tr>
               <td width="26">&nbsp;</td>
               <td width="18" background="images/index_r5_c3.jpg"><img src="images/index_r5_c3.jpg" alt="Bahamas freight forwarding and packing graphic" width="18" height="250" /></td>
               <td width="646" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="2" cellpadding="0">
                 <tr>
                   <td width="42%" valign="top"><p class="bodyCopy">&nbsp;</p>
                     <p class="bodyCopy"><img src="images/contactPic.jpg" width="241" height="190" /></p>
                     <blockquote>
                       <p class="bodyCopy"><strong>Security Forwarding Service Ltd.</strong><br />
                         Sixth Terrace Centreville<br />
                         P.O. Box N-9770<br />
                         Nassau, Bahamas <br />
                         </p>
                       <p class="bodyCopy"><strong>Telephone:<br />
                       </strong>242.325.8093 <br />
                         242.323.7221 </p>
                       <p class="bodyCopy"><strong>Fax:</strong>  <br />
                         242.356.6986</p>
                       <p class="bodyCopy"><strong>Web:<br />
                       </strong>www.BahamasBrokerage.com</p>
                     </blockquote></td>
                   <td width="58%" valign="top"><p><br />
                     <img src="images/titleContact.jpg" width="127" height="18" /></p>

					 <?php if  ($_GET["msgSent"] == "1"){?>
					  <?php
$emailBody="Hello,\n\n Name: {$_POST['firstName']} \n\n Company Name: {$_POST['companyName']} \n\n Enail: {$_POST['email']} \n\n Telephone: {$_POST['telephone']} \n\n Fax: {$_POST['fax']} \n\n Message: {$_POST['message']}";
mail('sfserviceltd@batelnet.bs', 'Mail From Website', $emailBody ,"From: '{$_POST['email']}'");
?>
                      <span class="bodyCopy style1">Thank you for filling out the form.  The message has been sent and somebody will review your message shortly.                      </span>
                      <?php } ?>
                     <p class="bodyCopy">Contact us for more information or to obtain a free estimate.</p>
                     <form action="contact.php?msgSent=1" method="post" name="form1" id="form1" onsubmit="MM_validateForm('firstName','','R','email','','RisEmail');return document.MM_returnValue">
                       <table width="91%" cellspacing="3" cellpadding="3">
                         <tr>
                           <td class="bodyCopy"><strong>Your Name:</strong></td>
                         </tr>
                         <tr>
                           <td><input name="firstName" type="text" id="firstName" />
                             <span class="copyright style2">*</span></td>
                         </tr>
                         <tr>
                           <td class="bodyCopy"><strong>Company Name </strong><em>(if applicable)</em><strong>: </strong></td>
                         </tr>
                         <tr>
                           <td><input name="companyName" type="text" id="companyName" /></td>
                         </tr>
                         <tr>
                           <td class="bodyCopy"><strong>Email Address: </strong></td>
                         </tr>
                         <tr>
                           <td><input name="email" type="text" id="email" />
                             <span class="copyright style2">*</span></td>
                         </tr>
                         <tr>
                           <td class="bodyCopy"><strong>Telephone:</strong></td>
                         </tr>
                         <tr>
                           <td><input name="telephone" type="text" id="telephone" /></td>
                         </tr>
                         <tr>
                           <td class="bodyCopy"><strong>Fax:</strong></td>
                         </tr>
                         <tr>
                           <td><input name="fax" type="text" id="fax" /></td>
                         </tr>
                         <tr>
                           <td class="bodyCopy"><strong>Specifics/Details of Request:</strong></td>
                         </tr>
                         <tr>
                           <td><textarea name="message" cols="42" rows="12" id="message"></textarea></td>
                         </tr>
                         <tr>
                           <td><div align="right">
                               <input type="submit" name="Submit" value="Send" />
                           </div></td>
                         </tr>
                         <tr>
                           <td><p align="center"><span class="copyright style2">*</span> <span class="bodyCopy">Indicates a required field</span></p>
                             <p>&nbsp;</p></td>
                         </tr>
                       </table>
                     </form>
                     <p></p>
                     <p class="bodyCopy">&nbsp;</p>                     </td>
                 </tr>
               </table></td>
               <td width="18" background="images/index_r5_c9.jpg"><img src="images/index_r5_c9.jpg" alt="Bahamas freight forwarding and packing graphic" width="18" height="250" /></td>
               <td width="39">&nbsp;</td>
             </tr>
             <tr>
               <td>&nbsp;</td>
               <td><img src="images/index_r6_c3.jpg" alt="Bahamas packing left bottom border" width="18" height="32" /></td>
               <td valign="top" bgcolor="#FFFFFF"><img src="images/index_r6_c4.jpg" alt="Bahamas Freight forwarding bottom border " width="646" height="32" /></td>
               <td><img src="images/index_r6_c9.jpg" alt="" width="18" height="32" /></td>
               <td>&nbsp;</td>
             </tr>
             <tr>
               <td colspan="5"><div align="center" class="copyright">All Content Copyright 2008 &copy; Security Forwarding Service Ltd.<br />
                 Any Duplication Without Express Written Consent is Strictly Prohibited.
</div></td>
               </tr>
           </table></td>
		  </tr>
		</table></td>
	  </tr>
	</table></td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-676538-9");
pageTracker._trackPageview();
</script>
</body>
</html>
